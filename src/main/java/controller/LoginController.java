package main.java.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import main.java.entity.Customer;
import main.java.exception.EmptyFieldException;
import main.java.model.Model;
import main.java.model.UserModel;

import java.io.IOException;

public class LoginController {
    @FXML
    Label exceptionLabel;
    @FXML
    TextField nameField = new TextField();
    @FXML
    TextField surnameField = new TextField();

    UserModel userModel = new UserModel();

    public static Customer customer;

    @FXML
    public void goBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/sample.fxml"));
        MyController.changeStage(root, "Music Store.");
    }

    @FXML
    public void onLogin() throws IOException {
        try {
            checkField();
        } catch (EmptyFieldException e) {
            exceptionLabel.setText(e.getMessage());
            exceptionLabel.setVisible(true);
            return;
        }
        userModel.addCustomer(nameField.getText(), surnameField.getText());
        Parent root = FXMLLoader.load(getClass().getResource("/scene_user1.2.fxml"));
        MyController.changeStage(root, "Music Store. User");
    }

    private void checkField() throws EmptyFieldException {
        if (nameField.getText().equals(""))
            throw new EmptyFieldException();
        if (surnameField.getText().equals(""))
            throw new EmptyFieldException();
    }
}
