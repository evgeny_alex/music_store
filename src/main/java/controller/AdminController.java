package main.java.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

import java.io.IOException;

public class AdminController {
    private static final String password = "0";

    @FXML
    private Label labelIncorrectPassword = new Label();

    @FXML
    private PasswordField passwordField = new PasswordField();

    @FXML
    public void tryLogin() throws IOException {
        if (!passwordField.getText().equals(password)) {
            labelIncorrectPassword.setText("Incorrect password. Try again.");
            passwordField.setText("");
        }
        else {
            Parent root = FXMLLoader.load(getClass().getResource("/scene_admin2.2.fxml"));
            MyController.changeStage(root, "Music Store. Admin");
        }
    }

    @FXML
    public void goBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/sample.fxml"));
        MyController.changeStage(root, "Music Store.");
    }
}
