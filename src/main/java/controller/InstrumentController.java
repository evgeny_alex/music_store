package main.java.controller;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import main.java.entity.Producer;
import main.java.entity.Type;
import main.java.exception.EmptyFieldException;
import main.java.model.Model;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.util.EmptyStackException;
import java.util.ResourceBundle;

public class InstrumentController implements Initializable {

    @FXML
    TextField modelText = new TextField();
    @FXML
    TextField typeText = new TextField();
    @FXML
    TextField producerText = new TextField();
    @FXML
    TextField costText = new TextField();
    @FXML
    DatePicker datePicker = new DatePicker();
    @FXML
    Label exceptionLabel = new Label();
    @FXML
    ChoiceBox<Type> typeBox = new ChoiceBox<>();
    @FXML
    ChoiceBox<Producer> producerBox = new ChoiceBox<>();

    Model model = new Model();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Type> types = FXCollections.observableArrayList(model.getAllType());
        typeBox.setItems(types);

        ObservableList<Producer> prods = FXCollections.observableArrayList(model.getAllProducers());
        producerBox.setItems(prods);
    }

    @FXML
    public void goBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/scene_admin2.1.fxml"));
        MyController.changeStage(root, "Music Store. Admin");
    }

    @FXML
    public void addType() {
        exceptionLabel.setText("");
        try {
            if (typeText.getText().equals(""))
                throw new EmptyFieldException();
        } catch (EmptyFieldException e) {
            exceptionLabel.setTextFill(Color.web("#ff0000"));
            exceptionLabel.setText(e.getMessage());
            return;
        }
        model.addType(typeText.getText());
        ObservableList<Type> types = FXCollections.observableArrayList(model.getAllType());
        typeBox.setItems(types);
        typeText.setText("");
    }

    @FXML
    public void addProducer() {
        exceptionLabel.setText("");
        try {
            if (producerText.getText().equals(""))
                throw new EmptyFieldException();
        } catch (EmptyFieldException e) {
            exceptionLabel.setTextFill(Color.web("#ff0000"));
            exceptionLabel.setText(e.getMessage());
            return;
        }
        model.addProducer(producerText.getText());
        ObservableList<Producer> prods = FXCollections.observableArrayList(model.getAllProducers());
        producerBox.setItems(prods);
        producerText.setText("");
    }

    @FXML
    public void addInstrument() {
        exceptionLabel.setText("");
        try {
            checkField();
        } catch (EmptyFieldException e) {
            exceptionLabel.setTextFill(Color.web("#ff0000"));
            exceptionLabel.setText(e.getMessage());
            return;
        }
        Integer cost = Integer.parseInt(costText.getText());
        model.addInstrument(modelText.getText(), typeBox.getValue().getId(), producerBox.getValue().getId(), cost, datePicker.getValue());
        exceptionLabel.setTextFill(Color.web("#00e600"));
        exceptionLabel.setText("Added to DB");
    }

    @FXML
    public void toStatistics() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/scene_admin2.3.fxml"));
        MyController.changeStage(root, "Music Store. Statistics");
    }

    private void checkField() throws EmptyFieldException {
        if (modelText.getText().equals(""))
            throw new EmptyFieldException();
        if (typeBox.getValue() == null)
            throw new EmptyFieldException();
        if (costText.getText().equals(""))
            throw new EmptyFieldException();
        if (producerBox.getValue() == null)
            throw new EmptyFieldException();
        if (datePicker.getValue().toString().equals(""))
            throw new EmptyFieldException();
    }


}
