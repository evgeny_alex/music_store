package main.java.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import main.java.entity.Type;
import main.java.exception.EmptyFieldException;
import main.java.model.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class StatisticController implements Initializable {
    @FXML
    TableView<StatName> tableName;
    @FXML
    TableColumn<StatName, String> columnName;
    @FXML
    TableColumn<StatName, String> columnSurname;
    @FXML
    TableView<StatType> tableType;
    @FXML
    TableColumn<StatType, String> columnType;
    @FXML
    TableColumn<StatType, String> columnSum;

    @FXML
    ChoiceBox<Type> typeBox1 = new ChoiceBox<>();
    @FXML
    ChoiceBox<String> monthBox = new ChoiceBox<>();
    @FXML
    ChoiceBox<String> costBox = new ChoiceBox<>();
    @FXML
    Label exceptionLabel = new Label();

    Model model = new Model();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Type> types1 = FXCollections.observableArrayList(model.getAllType());
        typeBox1.setItems(types1);

        ObservableList<String> month = FXCollections.observableArrayList("1 month", "2 month", "3 month");
        monthBox.setItems(month);

        ObservableList<String> cost = FXCollections.observableArrayList("1", "5", "10", "100", "1000");
        costBox.setItems(cost);

        tableName.setEditable(true);

        columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnSurname.setCellValueFactory(new PropertyValueFactory<>("surname"));

        tableType.setEditable(true);

        columnType.setCellValueFactory(new PropertyValueFactory<>("type"));
        columnSum.setCellValueFactory(new PropertyValueFactory<>("costSum"));
    }

    @FXML
    public void goBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/scene_admin2.2.fxml"));
        MyController.changeStage(root, "Music Store. Admin");
    }

    @FXML
    public void getStat1() {
        try {
            exceptionLabel.setVisible(false);
            checkEmptyFields1();
        } catch (EmptyFieldException e) {
            exceptionLabel.setVisible(true);
            return;
        }
        ObservableList<StatName> list = FXCollections.observableArrayList(model.getStatName(typeBox1.getValue(), monthBox.getValue(), costBox.getValue()));
        tableName.setItems(list);
    }

    @FXML
    public void getStat2() {
        ObservableList<StatType> list = FXCollections.observableArrayList(model.getStatType());
        tableType.setItems(list);
    }

    public void checkEmptyFields1() throws EmptyFieldException {
        if (typeBox1.getValue() == null)
            throw new EmptyFieldException();
        if (monthBox.getValue() == null)
            throw new EmptyFieldException();
        if (costBox.getValue() == null)
            throw new EmptyFieldException();
    }
}
