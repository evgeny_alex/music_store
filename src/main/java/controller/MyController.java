package main.java.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.java.application.MyApp;


import java.io.IOException;

public class MyController {

    @FXML
    public void onClickAdmin() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/scene_admin2.1.fxml"));
        changeStage(root, "Music Store. Admin");
    }

    @FXML
    public void onClickUser() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/scene_user1.1.fxml"));
        changeStage(root, "Music Store. User");
    }

    static void changeStage(Parent root, String title){
        MyApp.myStage.setTitle(title);
        MyApp.myStage.setScene(new Scene(root));
    }

}
