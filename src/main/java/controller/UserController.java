package main.java.controller;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import main.java.entity.Customer;
import main.java.model.TableRow;
import main.java.model.UserModel;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class UserController implements Initializable {
    @FXML
    TableView<TableRow> tableView;
    @FXML
    TableColumn<TableRow, Boolean> columnCheck;
    @FXML
    TableColumn<TableRow, String> columnModel;
    @FXML
    TableColumn<TableRow, String> columnDate;
    @FXML
    TableColumn<TableRow, String> columnType;
    @FXML
    TableColumn<TableRow, String> columnProducer;
    @FXML
    TableColumn<TableRow, String> columnCost;


    Customer customer = LoginController.customer;
    UserModel userModel = new UserModel();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tableView.setEditable(true);
        columnCheck.setCellValueFactory(param -> {
            TableRow row = param.getValue();
            SimpleBooleanProperty booleanProp = new SimpleBooleanProperty(false);

            booleanProp.addListener((observable, oldValue, newValue) -> {
                row.setCheck(newValue);
            });
            return booleanProp;
        });
        columnCheck.setCellFactory(p -> {
            CheckBoxTableCell<TableRow, Boolean> cell = new CheckBoxTableCell<>();
            cell.setAlignment(Pos.CENTER);
            return cell;
        });

        columnModel.setCellValueFactory(new PropertyValueFactory<>("model"));
        columnDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnType.setCellValueFactory(new PropertyValueFactory<>("type"));
        columnProducer.setCellValueFactory(new PropertyValueFactory<>("producer"));
        columnCost.setCellValueFactory(new PropertyValueFactory<>("cost"));
        fillTable();
    }

    @FXML
    public void goBack() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/scene_user1.1.fxml"));
        MyController.changeStage(root, "Music Store.");
    }

    @FXML
    public void buy() {
        ObservableList<TableRow> list = tableView.getItems();
        for (TableRow row : list) {
            if (row.getCheck())
                userModel.makePurchase(row, customer.getId());
        }
    }

    private void fillTable() {
        ObservableList<TableRow> list = FXCollections.observableArrayList(userModel.getCatalog());
        tableView.setItems(list);
    }
}
