package main.java.dao;

import java.sql.SQLException;
import java.util.List;

public interface GenericDAO<T> {
    void add(T t) throws SQLException;      // create
    List<T> getAll() throws SQLException;   // read
    T getById(int id);
    void update(T t);   // update
    void remove(T t);   // delete
}
