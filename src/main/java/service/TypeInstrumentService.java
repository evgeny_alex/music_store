package main.java.service;

import main.java.dao.GenericDAO;
import main.java.entity.TypeInstrument;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class TypeInstrumentService extends AbstractService implements GenericDAO<TypeInstrument> {

    Connection connection = getConnection();

    @Override
    public void add(TypeInstrument typeInstrument) throws SQLException {
        connection = getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO test.type_instrument (type_id, instrument_id) VALUES(?, ?)";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, typeInstrument.getTypeId());
            preparedStatement.setInt(2, typeInstrument.getInstrumentId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null)
                preparedStatement.close();
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public List<TypeInstrument> getAll() {
        return null;
    }

    @Override
    public TypeInstrument getById(int id) {
        return null;
    }

    @Override
    public void update(TypeInstrument typeInstrument) {

    }

    @Override
    public void remove(TypeInstrument typeInstrument) {

    }
}
