package main.java.service;

import main.java.dao.GenericDAO;
import main.java.entity.Inventory;

import java.sql.*;
import java.util.List;

public class InventoryService extends AbstractService implements GenericDAO<Inventory> {

    Connection connection = getConnection();

    @Override
    public void add(Inventory inventory) throws SQLException {
        connection = getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO test.inventory (instrument_id) VALUES(?)";
        String sql_select = "SELECT MAX(test.inventory.inventory_id) FROM test.inventory";

        Statement statement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, inventory.getInstrumentId());

            preparedStatement.executeUpdate();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_select);
            resultSet.next();
            inventory.setInventoryId(resultSet.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null)
                statement.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public List<Inventory> getAll() {
        return null;
    }

    @Override
    public Inventory getById(int id) {
        return null;
    }

    @Override
    public void update(Inventory inventory) {

    }

    @Override
    public void remove(Inventory inventory) {

    }
}
