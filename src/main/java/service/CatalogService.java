package main.java.service;

import main.java.dao.GenericDAO;
import main.java.model.TableRow;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CatalogService extends AbstractService implements GenericDAO<TableRow> {
    Connection connection = getConnection();

    @Override
    public void add(TableRow tableRow) throws SQLException {

    }

    @Override
    public List<TableRow> getAll() throws SQLException {
        connection = getConnection();

        List<TableRow> list = new ArrayList<>();

        String sql = "CALL getCatalog()";

        Statement statement = null;
        try {
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                TableRow row = new TableRow();
                row.setInstrumentId(resultSet.getInt(1));
                row.setModel(resultSet.getString(2));
                row.setDate(resultSet.getString(3));
                row.setType(resultSet.getString(4));
                row.setProducer(resultSet.getString(5));
                row.setCost(resultSet.getString(6));
                list.add(row);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return list;
    }

    @Override
    public TableRow getById(int id) {
        return null;
    }

    @Override
    public void update(TableRow tableRow) {

    }

    @Override
    public void remove(TableRow tableRow) {

    }
}
