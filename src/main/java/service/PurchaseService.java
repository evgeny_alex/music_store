package main.java.service;

import main.java.dao.GenericDAO;
import main.java.entity.Purchase;

import java.sql.*;
import java.util.List;

public class PurchaseService extends AbstractService implements GenericDAO<Purchase> {
    Connection connection = getConnection();

    @Override
    public void add(Purchase purchase) throws SQLException {
        connection = getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO test.purchase (inventory_id, customer_id, date) VALUES(?, ?, ?)";
        String sql_select = "SELECT MAX(test.purchase.inventory_id) FROM test.purchase";

        Statement statement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, purchase.getInventoryId());
            preparedStatement.setInt(2, purchase.getCustomerId());
            preparedStatement.setDate(3, new Date(new java.util.Date().getTime()));

            preparedStatement.executeUpdate();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_select);
            resultSet.next();
            purchase.setPurchaseId(resultSet.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null)
                statement.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public List<Purchase> getAll() throws SQLException {
        return null;
    }

    @Override
    public Purchase getById(int id) {
        return null;
    }

    @Override
    public void update(Purchase purchase) {

    }

    @Override
    public void remove(Purchase purchase) {

    }
}
