package main.java.service;

import main.java.entity.Instrument;
import main.java.dao.GenericDAO;

import java.sql.*;
import java.util.List;

public class InstrumentService extends AbstractService implements GenericDAO<Instrument> {

    Connection connection = getConnection();

    @Override
    public void add(Instrument instrument) throws SQLException {
        connection = getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO test.instrument (model, date, cost) VALUES(?, ?, ?)";
        String sql_select = "SELECT MAX(test.instrument.instrument_id) FROM test.instrument";

        Statement statement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);

            //preparedStatement.setInt(1, instrument.getId());
            preparedStatement.setString(1, instrument.getModel());
            preparedStatement.setDate(2, Date.valueOf(instrument.getDate()));
            preparedStatement.setInt(3, instrument.getCost());

            preparedStatement.executeUpdate();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_select);
            resultSet.next();
            instrument.setId(resultSet.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null)
                statement.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public List<Instrument> getAll() {
        return null;
    }

    @Override
    public Instrument getById(int id) {
        return null;
    }

    @Override
    public void update(Instrument instrument) {

    }

    @Override
    public void remove(Instrument instrument) {

    }
}
