package main.java.service;

import main.java.dao.GenericDAO;
import main.java.entity.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TypeService extends AbstractService implements GenericDAO<Type> {

    Connection connection = getConnection();

    @Override
    public void add(Type type) throws SQLException {
        connection = getConnection();

        String sql = "INSERT INTO test.type (name) VALUES(?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, type.getName());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public List<Type> getAll() throws SQLException {
        connection = getConnection();

        List<Type> typeList = new ArrayList<>();

        String sql = "SELECT name, type_id FROM test.type";

        Statement statement = null;
        try {
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Type type = new Type(resultSet.getString("name"));
                type.setId(resultSet.getInt("type_id"));
                typeList.add(type);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return typeList;
    }

    @Override
    public Type getById(int id) {
        return null;
    }

    @Override
    public void update(Type type) {

    }

    @Override
    public void remove(Type type) {

    }
}
