package main.java.service;

import main.java.dao.GenericDAO;
import main.java.entity.Producer;
import main.java.entity.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProducerService extends AbstractService implements GenericDAO<Producer> {

    Connection connection = getConnection();

    @Override
    public void add(Producer producer) throws SQLException {
        connection = getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO test.producer (name) VALUES(?)";

        try {
            preparedStatement = connection.prepareStatement(sql);

            //preparedStatement.setInt(1, instrument.getId());
            preparedStatement.setString(1, producer.getName());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null)
                preparedStatement.close();
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public List<Producer> getAll() throws SQLException {
        connection = getConnection();

        List<Producer> producerList = new ArrayList<>();

        String sql = "SELECT name, producer_id FROM test.producer";

        Statement statement = null;
        try {
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Producer type = new Producer(resultSet.getString("name"));
                type.setId(resultSet.getInt("producer_id"));
                producerList.add(type);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return producerList;
    }

    @Override
    public Producer getById(int id) {
        return null;
    }


    @Override
    public void update(Producer producer) {

    }

    @Override
    public void remove(Producer producer) {

    }
}
