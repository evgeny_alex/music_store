package main.java.service;

import main.java.entity.Type;
import main.java.model.StatName;
import main.java.model.StatType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StatisticService extends AbstractService {
    Connection connection = getConnection();

    public List<StatName> getStatName(Type type, String date, String cost) throws SQLException {
        connection = getConnection();
        List<StatName> list = new ArrayList<>();
        String sql = "CALL getStat1('" + type.getName() +  "', '" +  date + "', "+ cost + ")";

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery(sql);

            while (resultSet.next()) {
                StatName row = new StatName();
                row.setName(resultSet.getString(1));
                row.setSurname(resultSet.getString(2));
                list.add(row);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return list;
    }

    public List<StatType> getStatType() throws SQLException {
        connection = getConnection();
        List<StatType> list = new ArrayList<>();
        String sql = "CALL getStat2()";

        Statement statement = null;
        try {
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                StatType row = new StatType();
                row.setType(resultSet.getString(1));
                row.setCostSum(resultSet.getInt(2));
                list.add(row);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return list;
    }
}
