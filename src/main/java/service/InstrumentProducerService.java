package main.java.service;

import main.java.dao.GenericDAO;
import main.java.entity.InstrumentProducer;

import java.sql.*;
import java.util.List;

public class InstrumentProducerService extends AbstractService implements GenericDAO<InstrumentProducer> {

    Connection connection = getConnection();

    @Override
    public void add(InstrumentProducer instrumentProducer) throws SQLException {
        connection = getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO test.instrument_producer (instrument_id, producer_id) VALUES(?, ?)";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, instrumentProducer.getInstrumentId());
            preparedStatement.setInt(2, instrumentProducer.getProducerId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null)
                preparedStatement.close();
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public List<InstrumentProducer> getAll() {
        return null;
    }

    @Override
    public InstrumentProducer getById(int id) {
        return null;
    }

    @Override
    public void update(InstrumentProducer instrumentProducer) {

    }

    @Override
    public void remove(InstrumentProducer instrumentProducer) {

    }
}
