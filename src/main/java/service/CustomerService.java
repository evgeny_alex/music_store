package main.java.service;

import main.java.dao.GenericDAO;
import main.java.entity.Customer;

import java.sql.*;
import java.util.List;

public class CustomerService extends AbstractService implements GenericDAO<Customer> {
    Connection connection = getConnection();

    @Override
    public void add(Customer customer) throws SQLException {
        connection = getConnection();
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO test.customer (name, surname) VALUES(?, ?)";
        String sql_select = "SELECT MAX(test.customer.customer_id) FROM test.customer";

        Statement statement = null;

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, customer.getName());
            preparedStatement.setString(2, customer.getSurname());

            preparedStatement.executeUpdate();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql_select);
            resultSet.next();
            customer.setId(resultSet.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null)
                statement.close();
            if (preparedStatement != null)
                preparedStatement.close();
            if (connection != null)
                connection.close();
        }
    }

    @Override
    public List<Customer> getAll() throws SQLException {
        return null;
    }

    @Override
    public Customer getById(int id) {
        return null;
    }

    @Override
    public void update(Customer customer) {

    }

    @Override
    public void remove(Customer customer) {

    }
}
