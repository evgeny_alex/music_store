package main.java.application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.java.service.InstrumentService;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class MyApp extends Application {
    public static Stage myStage;

    public void startApp(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException, URISyntaxException {
        myStage = primaryStage;
        URL resource = getClass().getResource("/sample.fxml");
        Parent root = FXMLLoader.load(resource);
        primaryStage.setTitle("Music Store");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        MyApp app = new MyApp();
        app.startApp(args);
    }
}
