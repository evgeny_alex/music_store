package main.java.exception;

public class EmptyFieldException extends Throwable {
    @Override
    public String getMessage() {
        return "Empty some field. Please fill all fields.";
    }
}
