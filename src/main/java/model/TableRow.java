package main.java.model;

public class TableRow {
    private Boolean check = Boolean.FALSE;
    private String model;
    private String date;
    private String type;
    private String producer;
    private String cost;
    private int instrument_id;

    public int getInstrumentId() {
        return instrument_id;
    }

    public void setInstrumentId(int instrument_id) {
        this.instrument_id = instrument_id;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setDate(String data) {
        this.date = data;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public String getCost() {
        return cost;
    }

    public String getDate() {
        return date;
    }

    public String getProducer() {
        return producer;
    }

    public String getType() {
        return type;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public Boolean getCheck() {
        return check;
    }
}
