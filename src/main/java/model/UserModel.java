package main.java.model;

import main.java.controller.LoginController;
import main.java.entity.Customer;
import main.java.entity.Inventory;
import main.java.entity.Purchase;
import main.java.service.CatalogService;
import main.java.service.CustomerService;
import main.java.service.InventoryService;
import main.java.service.PurchaseService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserModel {
    private InventoryService inventoryService = new InventoryService();
    private PurchaseService purchaseService = new PurchaseService();
    private CustomerService customerService = new CustomerService();
    private CatalogService catalogService = new CatalogService();

    public void addCustomer(String name, String surname) {
        try {
            Customer customer = new Customer(name, surname);
            customerService.add(customer);
            LoginController.customer = customer;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<TableRow> getCatalog() {
        List<TableRow> list = new ArrayList<>();
        try {
            list = catalogService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void makePurchase(TableRow row, int customerId) {
        try {
            Inventory inventory = new Inventory();
            inventory.setInstrumentId(row.getInstrumentId());
            inventoryService.add(inventory);

            Purchase purchase = new Purchase(inventory.getInventoryId(), customerId);
            purchaseService.add(purchase);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
