package main.java.model;

import main.java.entity.*;
import main.java.service.*;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Model {
    private InstrumentService instrumentService = new InstrumentService();
    private InstrumentProducerService instrumentProducerService = new InstrumentProducerService();
    private ProducerService producerService = new ProducerService();
    private TypeService typeService = new TypeService();
    private TypeInstrumentService typeInstrumentService = new TypeInstrumentService();
    private StatisticService statisticService = new StatisticService();

    public void addInstrument(String model, int typeId, int producerId, Integer cost, LocalDate date) {
        try {
            // Добавление в таблицу instrument
            Instrument instrument = new Instrument(model, date, cost);
            instrumentService.add(instrument);

            // Добавление в промежуточную таблицу instrument_producer
            InstrumentProducer instrumentProducer = new InstrumentProducer(instrument.getId(), producerId);
            instrumentProducerService.add(instrumentProducer);

            // Добавление в промежуточную таблицу type_instrument
            TypeInstrument typeInstrument = new TypeInstrument(typeId, instrument.getId());
            typeInstrumentService.add(typeInstrument);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addType(String typeName) {
        try {
            // Добавление в таблицу type
            Type type = new Type(typeName);
            typeService.add(type);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addProducer(String producerName) {
        try {
            // Добавление в таблицу producer
            Producer producer = new Producer(producerName);
            producerService.add(producer);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Type> getAllType() {
        List<Type> typeList = new ArrayList<>();
        try {
            typeList = typeService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return typeList;
    }

    public List<Producer> getAllProducers() {
        List<Producer> producerList = new ArrayList<>();
        try {
            producerList = producerService.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return producerList;
    }

    public List<StatName> getStatName(Type type, String date, String cost) {
        List<StatName> list = new ArrayList<>();
        String dateIn = "";

        switch (date) {
            case "1 month":
                dateIn = "2020-04-01";
                break;
            case "2 month":
                dateIn = "2020-03-01";
                break;
            default:
                dateIn = "2020-02-01";
                break;
        }
        try {
            list = statisticService.getStatName(type, dateIn, cost);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<StatType> getStatType() {
        List<StatType> list = new ArrayList<>();
        try {
            list = statisticService.getStatType();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

}
