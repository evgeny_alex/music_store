package main.java.model;

public class StatType {
    private String type;
    private int costSum;

    public String getType() {
        return type;
    }

    public int getCostSum() {
        return costSum;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCostSum(int costSum) {
        this.costSum = costSum;
    }
}
