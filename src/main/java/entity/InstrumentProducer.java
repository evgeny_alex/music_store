package main.java.entity;

public class InstrumentProducer {
    private int instrumentId;
    private int producerId;

    public InstrumentProducer(int instrumentId, int producerId) {
        this.instrumentId = instrumentId;
        this.producerId = producerId;
    }

    public int getInstrumentId() {
        return instrumentId;
    }

    public int getProducerId() {
        return producerId;
    }
}
