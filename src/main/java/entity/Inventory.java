package main.java.entity;

public class Inventory {
    private int inventoryId;
    private int instrumentId;

    public int getInstrumentId() {
        return instrumentId;
    }

    public int getInventoryId() {
        return inventoryId;
    }

    public void setInstrumentId(int instrumentId) {
        this.instrumentId = instrumentId;
    }

    public void setInventoryId(int inventoryId) {
        this.inventoryId = inventoryId;
    }
}
