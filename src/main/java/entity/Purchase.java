package main.java.entity;

public class Purchase {
    private int purchaseId;
    private int inventoryId;
    private int customerId;

    public Purchase(int inventoryId, int customerId) {
        this.inventoryId = inventoryId;
        this.customerId = customerId;
    }

    public int getInventoryId() {
        return inventoryId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }
}
