package main.java.entity;

public class TypeInstrument {
    private int typeId;
    private int instrumentId;

    public TypeInstrument(int typeId, int instrumentId) {
        this.typeId = typeId;
        this.instrumentId = instrumentId;
    }

    public int getInstrumentId() {
        return instrumentId;
    }

    public int getTypeId() {
        return typeId;
    }
}
