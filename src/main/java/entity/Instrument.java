package main.java.entity;

import java.sql.Date;
import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

public class Instrument {
    private Integer id;
    private String model;
    private LocalDate date;
    private Integer cost;

    public Instrument(String model, LocalDate date, Integer cost) {
        this.model = model;
        this.date = date;
        this.cost = cost;
    }

    public Integer getCost() {
        return cost;
    }

    public String getModel() {
        return model;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
